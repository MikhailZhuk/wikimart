package wikimart.test.zhukm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wikimart.test.zhukm.commons.Offer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class OldXmlReader {
    private static Logger log = LoggerFactory.getLogger(OldXmlReader.class);
    private static Map<Integer, Offer> offerById = new HashMap<>();
    private static String xml;

    public OldXmlReader(String xml) {
        this.xml = xml;
        try {
            offerById = readXml();
        } catch (XMLStreamException | IOException | JAXBException e) {
            log.error("Reading old xml error: ", e);
        }
    }

    public static Map<Integer, Offer> getOfferById() {
        return offerById;
    }

    private static Map<Integer, Offer> readXml() throws XMLStreamException, IOException, JAXBException {
        HashMap<Integer, Offer> offerById = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(xml))) {
            String line;
            StringBuilder sb = new StringBuilder();
            boolean readingOffer = false;
            while ((line = br.readLine()) != null) {
                if (readingOffer) {
                    if (line.trim().equals("</offer>")) {
                        sb.append(line);
                        JAXBContext jaxbContext = JAXBContext.newInstance(Offer.class);
                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                        Offer offer = (Offer) jaxbUnmarshaller.unmarshal(new StringReader(sb.toString()));
                        offer.setRemoved(true);
                        offerById.put(offer.getId(), offer);
                        sb = new StringBuilder();
                        readingOffer = false;
                    } else {
                        sb.append(line);
                    }
                } else if (line.trim().startsWith("<offer ")) {
                    readingOffer = true;
                    sb.append(line);
                }
            }
        }
        return offerById;
    }

    public static void compare(Offer newOffer) {
        Offer oldOffer = offerById.get(newOffer.getId());
        if (oldOffer != null) {
            if (!oldOffer.equals(newOffer)) {
                newOffer.setModified(true);
            }
            offerById.remove(newOffer.getId());
        } else {
            newOffer.setNew(true);
        }
    }
}