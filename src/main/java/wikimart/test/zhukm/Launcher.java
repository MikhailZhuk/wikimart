package wikimart.test.zhukm;

import wikimart.test.zhukm.commons.Offer;
import wikimart.test.zhukm.util.Util;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.concurrent.*;

public class Launcher {
    private static String newXml = "yandex.xml";
    private static String oldXml = "yandex1.xml";
    private static final int URL_CHECKERS_COUNT = 10;

    public static void main(String[] args) throws InterruptedException, XMLStreamException, IOException {
        newXml = args[0];
        oldXml = args[1];
        long startTime = System.currentTimeMillis();
        Semaphore semaphore = new Semaphore(URL_CHECKERS_COUNT);
        NewXmlReaderTask newReaderTask = new NewXmlReaderTask(newXml, oldXml, semaphore);
        Thread newReaderThread = new Thread(newReaderTask);
        newReaderThread.start();
        ExecutorService threadPool = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });
        ConcurrentMap<Integer, Offer> npmCheckedOffers = new ConcurrentHashMap<>();
        for (int i = 0; i < URL_CHECKERS_COUNT; i++) {
            threadPool.execute(new PictureCheckTask(semaphore, newReaderTask.getOffers(), npmCheckedOffers));
        }
        newReaderThread.join();
        long endTime = System.currentTimeMillis();
        Util.printMap(npmCheckedOffers);
        Util.printMap(OldXmlReader.getOfferById());
        System.out.println("----------------------------------------------");
        System.out.println("Time elapsed: " + (endTime - startTime) / 1000);
    }
}