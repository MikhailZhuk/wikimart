package wikimart.test.zhukm.commons;

import javax.xml.bind.annotation.*;
import java.util.HashSet;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Offer {
    @XmlAttribute
    private int id;
    @XmlAttribute
    private String type;
    private String price;
    private String oldprice;
    private String url;
    private String categoryId;
    private String currencyId;
    private String picture;
    private String model;
    @XmlElement(name = "typeprefix")
    private String typePrefix;
    private String vendor;
    @XmlElement(name = "vendorcode")
    private String vendorCode;
    private String description;
    @XmlElement(name = "country_of_origin")
    private String countryOfOrigin;
    private String accessory;
    @XmlElement(name = "manufacturer_warranty")
    private boolean manufacturerWarranty;
    private String stock;
    @XmlElement(name = "local_delivery_cost")
    private String localDeliveryCost;
    @XmlElement(name = "wikimart_delivery_cost")
    private String wikimartDeliveryCost;
    @XmlElement(name = "param")
    private HashSet<Param> params;
    @XmlTransient
    private boolean isNew;
    @XmlTransient
    private boolean isModified;
    @XmlTransient
    private boolean isRemoved;
    @XmlTransient
    private boolean isPicProblem;

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }

    public boolean isPicProblem() {
        return isPicProblem;
    }

    public void setPicProblem(boolean picProblem) {
        isPicProblem = picProblem;
    }

    public int getId() {
        return id;
    }

    public String getPicture() {
        return picture;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append(id).append(" ");
        if (isNew) builder.append("n");
        if (isModified) builder.append("m");
        if (isPicProblem) builder.append("p");
        if (isRemoved) builder.append("r");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offer offer = (Offer) o;
        if (id != offer.id) return false;
        if (manufacturerWarranty != offer.manufacturerWarranty) return false;
        if (type != null ? !type.equals(offer.type) : offer.type != null) return false;
        if (price != null ? !price.equals(offer.price) : offer.price != null) return false;
        if (oldprice != null ? !oldprice.equals(offer.oldprice) : offer.oldprice != null) return false;
        if (url != null ? !url.equals(offer.url) : offer.url != null) return false;
        if (categoryId != null ? !categoryId.equals(offer.categoryId) : offer.categoryId != null) return false;
        if (currencyId != null ? !currencyId.equals(offer.currencyId) : offer.currencyId != null) return false;
        if (picture != null ? !picture.equals(offer.picture) : offer.picture != null) return false;
        if (model != null ? !model.equals(offer.model) : offer.model != null) return false;
        if (typePrefix != null ? !typePrefix.equals(offer.typePrefix) : offer.typePrefix != null) return false;
        if (vendor != null ? !vendor.equals(offer.vendor) : offer.vendor != null) return false;
        if (vendorCode != null ? !vendorCode.equals(offer.vendorCode) : offer.vendorCode != null) return false;
        if (description != null ? !description.equals(offer.description) : offer.description != null) return false;
        if (countryOfOrigin != null ? !countryOfOrigin.equals(offer.countryOfOrigin) : offer.countryOfOrigin != null)
            return false;
        if (accessory != null ? !accessory.equals(offer.accessory) : offer.accessory != null) return false;
        if (stock != null ? !stock.equals(offer.stock) : offer.stock != null) return false;
        if (localDeliveryCost != null ? !localDeliveryCost.equals(offer.localDeliveryCost) : offer.localDeliveryCost != null)
            return false;
        if (wikimartDeliveryCost != null ? !wikimartDeliveryCost.equals(offer.wikimartDeliveryCost) : offer.wikimartDeliveryCost != null)
            return false;
        return params != null ? params.equals(offer.params) : offer.params == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (oldprice != null ? oldprice.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
        result = 31 * result + (currencyId != null ? currencyId.hashCode() : 0);
        result = 31 * result + (picture != null ? picture.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (typePrefix != null ? typePrefix.hashCode() : 0);
        result = 31 * result + (vendor != null ? vendor.hashCode() : 0);
        result = 31 * result + (vendorCode != null ? vendorCode.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (countryOfOrigin != null ? countryOfOrigin.hashCode() : 0);
        result = 31 * result + (accessory != null ? accessory.hashCode() : 0);
        result = 31 * result + (manufacturerWarranty ? 1 : 0);
        result = 31 * result + (stock != null ? stock.hashCode() : 0);
        result = 31 * result + (localDeliveryCost != null ? localDeliveryCost.hashCode() : 0);
        result = 31 * result + (wikimartDeliveryCost != null ? wikimartDeliveryCost.hashCode() : 0);
        result = 31 * result + (params != null ? params.hashCode() : 0);
        return result;
    }
}