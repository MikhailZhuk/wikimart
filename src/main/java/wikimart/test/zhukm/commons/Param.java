package wikimart.test.zhukm.commons;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "param")
public class Param implements Comparable {

    @Override
    public int compareTo(Object o) {
        return ((Param) o).getParamName().compareTo(paramName);
    }

    @XmlAttribute(name = "name")
    private String paramName;
    @XmlValue
    private String value;

    public String getParamName() {
        return paramName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Param param = (Param) o;
        if (paramName != null ? !paramName.equals(param.paramName) : param.paramName != null) return false;
        return value != null ? value.equals(param.value) : param.value == null;
    }

    @Override
    public int hashCode() {
        int result = paramName != null ? paramName.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Param{" +
                "paramName='" + paramName + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}