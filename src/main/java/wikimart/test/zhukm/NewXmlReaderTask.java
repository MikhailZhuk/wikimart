package wikimart.test.zhukm;

import wikimart.test.zhukm.commons.Offer;
import wikimart.test.zhukm.util.Util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class NewXmlReaderTask implements Runnable {
    private String newXml;
    private String oldXml;
    private BlockingQueue<Offer> offers = new LinkedBlockingQueue<>();
    private Semaphore semaphore;
    private int workersToWaitCount;

    public NewXmlReaderTask(String newInFile, String oldInFile, Semaphore semaphore) {
        newXml = Util.convertToHomePath(newInFile);
        oldXml = Util.convertToHomePath(oldInFile);
        workersToWaitCount = semaphore.availablePermits();
        this.semaphore = semaphore;
    }

    public BlockingQueue<Offer> getOffers() {
        return offers;
    }

    @Override
    public void run() {
        try {
            readNewOffers();
        } catch (InterruptedException | IOException | XMLStreamException | JAXBException e) {
            e.printStackTrace();
        }
    }

    public void readNewOffers() throws XMLStreamException, IOException, InterruptedException, JAXBException {
        OldXmlReader oldXmlReader = new OldXmlReader(oldXml);
        try (BufferedReader br = new BufferedReader(new FileReader(newXml))) {
            String line;
            StringBuilder sb = new StringBuilder();
            boolean readingOffer = false;
            while ((line = br.readLine()) != null) {
                if (readingOffer) {
                    if (line.trim().equals("</offer>")) {
                        sb.append(line);
                        JAXBContext jaxbContext = JAXBContext.newInstance(Offer.class);
                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                        Offer offer = (Offer) jaxbUnmarshaller.unmarshal(new StringReader(sb.toString()));
                        oldXmlReader.compare(offer);
                        offers.add(offer);
                        sb = new StringBuilder();
                        readingOffer = false;
                    } else {
                        sb.append(line);
                    }
                } else if (line.trim().startsWith("<offer ")) {
                    semaphore.acquire();
                    readingOffer = true;
                    sb.append(line);
                }
            }
        }
        semaphore.acquire(workersToWaitCount);
    }
}