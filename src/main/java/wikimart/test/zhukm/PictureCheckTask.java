package wikimart.test.zhukm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wikimart.test.zhukm.commons.Offer;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;

public class PictureCheckTask implements Runnable {
    private Logger log = LoggerFactory.getLogger(PictureCheckTask.class);
    private static final int TIMES_TRY_TO_CONNECT = 40;
    private static final int CONNECTION_TIMEOUT_MS = 150;
    private static final int READ_TIMEOUT_MS = 50;
    private static final int PAUSE_BETWEEN_CONNECTIONS_MS = 5;
    private Semaphore semaphore;
    private BlockingQueue<Offer> offers;
    private ConcurrentMap<Integer, Offer> npmOfferById;

    public PictureCheckTask(Semaphore semaphore, BlockingQueue<Offer> offers, ConcurrentMap<Integer, Offer> npmCheckedOffers) {
        this.semaphore = semaphore;
        this.offers = offers;
        this.npmOfferById = npmCheckedOffers;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Offer offer = offers.take();
                offer.setPicProblem(isPictureProblem(offer.getPicture()));
                if (offer.isNew() || offer.isModified() || offer.isPicProblem()) {
                    npmOfferById.put(offer.getId(), offer);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            semaphore.release();
        }
    }

    private boolean isPictureProblem(String url) throws InterruptedException {
        return url == null || !isValid(url);
    }

    private boolean isValid(String url) throws InterruptedException {
        URL u;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            return false;
        }
        int code;
        for (int i = 0; i < TIMES_TRY_TO_CONNECT; i++) {
            try {
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setInstanceFollowRedirects(false);
                huc.setRequestMethod("GET");
                huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
                huc.setConnectTimeout(CONNECTION_TIMEOUT_MS);
                huc.setReadTimeout(READ_TIMEOUT_MS);
                huc.connect();
                code = huc.getResponseCode();
                if (code != 0 && code == 200) {
                    return true;
                }
            } catch (SocketTimeoutException e) {
                log.info("Connecting timeout: ", e);
            } catch (Exception e) {
                log.info("Connecting exception: ", e);
                return false;
            }
            if (i != TIMES_TRY_TO_CONNECT - 1) {
                Thread.sleep(PAUSE_BETWEEN_CONNECTIONS_MS);
            }
        }
        return false;
    }
}