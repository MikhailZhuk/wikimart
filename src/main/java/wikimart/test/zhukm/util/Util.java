package wikimart.test.zhukm.util;

import wikimart.test.zhukm.commons.Offer;

import java.util.Map;

public class Util {

    public static void printMap(Map<Integer,Offer> offerById){
        for (Map.Entry<Integer, Offer> entry : offerById.entrySet()) {
            System.out.println(entry.getValue());
        }
    }

    public static String convertToHomePath(String path){
        String[] split = path.split("/");
        String newFile = split[split.length - 1];
        return System.getProperty("user.home") + "/" + newFile;
    }
}